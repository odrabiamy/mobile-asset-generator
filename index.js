const fs = require("fs")
const path = require("path")
const Sharp = require("sharp")

function generateIcons({pathToFile, system, width, height}) {

    const file = path.basename(pathToFile)
    const filename = file.substring(0, file.length - 4)

    if (system === "default" || system === "android") {
        const androidScales = [1, 1.5, 2, 3, 4]
        const androidFolders = [
            "drawable-mdpi",
            "drawable-hdpi",
            "drawable-xhdpi",
            "drawable-xxhdpi",
            "drawable-xxxhdpi",
        ]

        const androidFolder = path.join(__dirname, ".", `android`)

        if (!fs.existsSync(`${androidFolder}`)) {
            fs.mkdirSync(`${androidFolder}`)
        }

        for (let i = 0;i < androidFolders.length;i++) {
            const folder = androidFolders[i]
            const scale = androidScales[i]
            const widthScale = width * scale
            const heightScale = height * scale

            if (!fs.existsSync(`${androidFolder}/${folder}`)) {
                fs.mkdirSync(`${androidFolder}/${folder}`)
            }

            createIcon({pathToFile, scale: androidScales[i], filename: `${androidFolder}/${folder}/${filename}.png`, width: widthScale, height: heightScale})
        }
    }

    if (system === "default" || system === "ios") {
        const iosScales = [1, 2, 3]
        const iosPostFix = ["", "@2x", "@3x"]

        const iosFolder = path.join(
            __dirname,
            ".",
            `ios`
        )

        if (!fs.existsSync(iosFolder)) {
            fs.mkdirSync(iosFolder)
        }

        if (!fs.existsSync(`${iosFolder}/${filename}.imageset`)) {
            fs.mkdirSync(`${iosFolder}/${filename}.imageset`)
        }

        	const contents  = `{
    "images" : [
      {
        "filename" : "${filename}.png",
        "idiom" : "universal",
        "scale" : "1x"
      },
      {
        "filename" : "${filename}@2x.png",
        "idiom" : "universal",
        "scale" : "2x"
      },
      {
        "filename" : "${filename}@3x.png",
        "idiom" : "universal",
        "scale" : "3x"
      }
    ],
    "info" : {
      "author" : "xcode",
      "version" : 1
    }
  }
  `;
		fs.writeFileSync(`${iosFolder}/${filename}.imageset/Contents.json`, contents);

        for (let i = 0;i < iosScales.length;i++) {
            const widthScale = width * iosScales[i]
            const heightScale = height * iosScales[i]

            createIcon({pathToFile, scale: iosScales[i], filename: `${iosFolder}/${filename}.imageset/${filename}${iosPostFix[i]}.png`, width: widthScale, height: heightScale})
        }
    }
}

function createIcon({pathToFile, scale, filename, width, height}) {
    Sharp(pathToFile, { density: Math.round(72 * scale) })
        .resize( Math.round(width), Math.round(height))
        .png()
        .toFile(filename)
}

function createIcons({path, system = "default", width, height = width}) {
    const pathInfo = fs.lstatSync(path)
    if (pathInfo.isDirectory()) {
        if (path[path.length - 1] !== "/") {
            path = path + "/"
        }

        const files = fs.readdirSync(path)

        for (const file of files) {
            generateIcons({pathToFile: path + file, system, width, height})
        }
    } else if (pathInfo.isFile()) {
        generateIcons({pathToFile, system, width, height})
    }
}

// path = path to svgs or svg
// width = default width of the icons
// height = default height of the icons
// system = default, android, ios
// run = yarn run start


createIcons({ path:"./svgs", width: 175, height: 30})
